//
//  DetailHeroViewController.swift
//  HerosMarvel
//
//  Created by André Feliciano on 29/04/21.
//

import UIKit
import WebKit

class DetailHeroViewController: UIViewController {
    
    @IBOutlet weak var detailHeroWebView: WKWebView!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    
    var hero: HeroModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = hero.name
        
        guard let urlString = hero.urls.first?.url else { return }
        guard let url = URL(string: urlString) else { return }
        let request = URLRequest(url: url)
        
        configureWebView(withRequest: request)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadingActivity.isHidden = false
        loadingActivity.startAnimating()
    }
    
    private func configureWebView(withRequest: URLRequest) {
        detailHeroWebView.allowsBackForwardNavigationGestures = true
        detailHeroWebView.navigationDelegate = self
        detailHeroWebView.load(withRequest)
    }
}


// MARK: WKNavigationDelegate
extension DetailHeroViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadingActivity.stopAnimating()
        loadingActivity.isHidden = true
    }
}
