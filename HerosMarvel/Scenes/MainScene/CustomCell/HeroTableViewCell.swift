//
//  HeroTableViewCell.swift
//  HerosMarvel
//
//  Created by André Feliciano on 29/04/21.
//

import UIKit
import Kingfisher

class HeroTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func prepareCell(with hero: HeroModel) {
        nameLabel.text = hero.name
        descriptionLabel.text = hero.description
        
        let thumbnailURL = hero.thumbnail.url
        if let url = URL(string: thumbnailURL) {
            thumbnailImageView.kf.indicatorType = .activity
            thumbnailImageView.kf.setImage(with: url)
        } else {
            thumbnailImageView.image = nil
        }
        thumbnailImageView.clipsToBounds = true
        thumbnailImageView.layer.cornerRadius = thumbnailImageView.frame.size.width * 0.2
        thumbnailImageView.layer.borderWidth = 2.0
        thumbnailImageView.layer.borderColor = UIColor(cgColor: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)).cgColor
    }
}
