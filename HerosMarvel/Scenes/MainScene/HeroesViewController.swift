//
//  HeroesViewController.swift
//  HerosMarvel
//
//  Created by André Feliciano on 28/04/21.
//

import UIKit

class HeroesViewController: UIViewController {

    @IBOutlet weak var herosTableView: UITableView!
    
    var listHeroes: [HeroModel] = []
    var currentPage = 0
    var totalRecors = 0
    var loadingHeroes = false
    var label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = "Por favor, aguarde.\nBuscando lista de herois..."
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        loadHeros()
    }
    
    private func setupTableView() {
        herosTableView.delegate = self
        herosTableView.dataSource = self
        let nib = UINib(nibName: "HeroTableViewCell", bundle: nil)
        herosTableView.register(nib, forCellReuseIdentifier: "HeroTableViewCell")
    }
    
    private func loadHeros() {
        loadingHeroes = true
        MarvelAuthorizing.loadHeros(page: currentPage) { (marvelInfo) in
            self.listHeroes += marvelInfo.data.results
            self.totalRecors = marvelInfo.data.total
            DispatchQueue.main.async {
                self.loadingHeroes = false
                self.label.text = "Não foi possivél carregar a lista de heróis!"
                self.herosTableView.reloadData()
            }
        } onError: { requestError in
            print("\n\nOcorreu erro: \(requestError).")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! DetailHeroViewController
        guard let index = herosTableView.indexPathForSelectedRow?.row else { return }
        vc.hero = listHeroes[index]
    }
}


// MARK: UITableViewDelegate
extension HeroesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailHeroSegue", sender: nil)
        herosTableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == listHeroes.count - 15 && !loadingHeroes && listHeroes.count != totalRecors {
            currentPage += 1
            loadHeros()
        }
    }
}


// MARK: UITableViewDataSource
extension HeroesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        herosTableView.backgroundView = listHeroes.count == 0 ? label : nil
        return listHeroes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeroTableViewCell", for: indexPath) as? HeroTableViewCell
        cell?.prepareCell(with: listHeroes[indexPath.row])
        return cell ?? UITableViewCell()
    }
}
