//
//  MarvelAuthorizing.swift
//  HerosMarvel
//
//  Created by André Feliciano on 28/04/21.
//

import Foundation
import SwiftHash

enum RequestError {
    case noResponse
    case invalidStatusCode(statusCode: Int)
    case noData
    case errorDecoderJson
    case appError(error: Error)
}

class MarvelAuthorizing {
    static private let basePath = "https://gateway.marvel.com/v1/public/characters?"
    static private let publicKey = "1a86e7575a06f8f28bab1531ad57906f"
    static private let privateKey = "46745326d601a08c589130f0fe0a3b63b7c016bb"
    static private let limit = 50
    
    private class func getAuthentication() -> String {
        let ts = String(Date().timeIntervalSince1970)
        let hash = MD5(ts+privateKey+publicKey).lowercased()
        return "ts=\(ts)&apikey=\(publicKey)&hash=\(hash)"
    }
    
    private static let configuration: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        config.allowsCellularAccess = true
        config.httpAdditionalHeaders = ["Content-Type": "application/json"]
        config.timeoutIntervalForRequest = 30.0
        config.httpMaximumConnectionsPerHost = 5
        return config
    }()
    private static let session = URLSession(configuration: configuration)
    
    class func loadHeros(page: Int = 0, onComplete: @escaping (MarvelInfoModel) -> Void, onError: @escaping (RequestError) -> Void) {
        let offset = page * limit
        let urlString = basePath + "offset=\(offset)&limit=\(limit)&" + getAuthentication()
        guard let url = URL(string: urlString) else { return }
        
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            
            if error == nil {
                guard let response = response as? HTTPURLResponse else {
                    onError(.noResponse)
                    return
                }
                
                if response.statusCode == 200 {
                    guard let data = data else {
                        onError(.noData)
                        return
                    }
                    
                    do {
                        let heros = try JSONDecoder().decode(MarvelInfoModel.self, from: data)
                        onComplete(heros)
                    } catch {
                        onError(.errorDecoderJson)
                    }
                } else {
                    onError(.invalidStatusCode(statusCode: response.statusCode))
                }
                
            } else {
                onError(.appError(error: error!))
            }
        }
        dataTask.resume()
    }
}
