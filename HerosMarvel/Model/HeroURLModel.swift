//
//  HeroURLModel.swift
//  HerosMarvel
//
//  Created by André Feliciano on 28/04/21.
//

import Foundation

struct HeroURLModel: Codable {
    let type: String
    let url: String
}
