//
//  MarvelInfoModel.swift
//  HerosMarvel
//
//  Created by André Feliciano on 28/04/21.
//

import Foundation

struct MarvelInfoModel: Codable {
    let code: Int
    let status: String
    let copyright: String
    let attributionText: String
    let data: MarvelInfoDataModel
}
