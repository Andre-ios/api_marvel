//
//  MarvelInfoDataModel.swift
//  HerosMarvel
//
//  Created by André Feliciano on 28/04/21.
//

import Foundation

struct MarvelInfoDataModel: Codable {
    let offset: Int
    let limit : Int
    let total: Int
    let count: Int
    let results: [HeroModel]
}
