//
//  ThumbnailModel.swift
//  HerosMarvel
//
//  Created by André Feliciano on 28/04/21.
//

import Foundation

struct ThumbnailModel: Codable {
    let path: String
    let ext: String
    
    var url: String {
        return path + "." + ext
    }
    
    enum CodingKeys: String, CodingKey {
        case path
        case ext = "extension"
    }
}
