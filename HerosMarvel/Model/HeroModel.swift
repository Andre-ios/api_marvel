//
//  HeroModel.swift
//  HerosMarvel
//
//  Created by André Feliciano on 28/04/21.
//

import Foundation

struct HeroModel: Codable {
    let id: Int
    let name: String
    let description: String
    let thumbnail: ThumbnailModel
    let urls: [HeroURLModel]
}
